#!/bin/bash

set -e

mkdir -p $HOME/scripts

if [ ! -f "$HOME/scripts/dev.sh" ]; then
  git clone https://gitlab.com/bassforce86/scripts.git $HOME/scripts/
fi

latesttag=$(git tag --list '[vV]*' --sort=v:refname |tail -1)
echo "checking out ${latesttag}"
git checkout "${latesttag}"

chmod +x $HOME/scripts/dev.sh

echo 'alias dev="$HOME/scripts/dev.sh"' >> ~/.bashrc
source ~/.bashrc
