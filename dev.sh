#!/bin/bash
# ------------------------------------------------------------------
# [Author] Docker helpers
#          Helper scripts for docker-compose in the dev environment
# ------------------------------------------------------------------

project="<project>" # Default to empty project
command="<command>" # Default to empty command
depth=5             # Default to a depth of 2 for performance
all_projects=false  # Default all projects to false
alias="./dev.sh"    # Default alias is ./dev.sh

# --- Helpers --- #

function script_help() {
  general_help
  help_notes
  project_help
  container_help
}

function proj_help() {
  echo "Usage:"
  help_notes
  project_help
  container_help
}

function cont_help() {
  echo "Usage:"
  help_notes
  container_help
}

function general_help() {
  echo "Usage:"
  echo "$alias -h                  Display this help message."
  echo ""
}

function help_notes() {
  echo "##### Notes: "
  echo "- <project> refers to the project folder holding the docker-compose.yml"
  echo "- If you're already in the project folder use: . "
  echo ""
}

function project_help() {
  echo ""
  echo "Project Commands:"
  echo "$alias -a <command> $project        Perform command on a list of projects"
  echo ""
  echo "$alias up $project                  Spin up project:    docker-compose up <project> -d"
  echo "$alias dn, down $project            Spin down project:  docker-compose down <project>"
  echo "$alias b, build $project            Build project:      docker-compose build <project>"
  echo "$alias rs, restart $project         Restart Project:    docker-compose down <project> && docker-compose up <project> -d"
  echo "$alias rb, rebuild $project         Rebuild Project:    docker-compose down <project> && docker-compose build && docker-compose up <project> -d"
  echo "$alias ls, list $project            List Services:      docker-compose ps --services"
  echo "$alias nuke $project                Removes all containers, volumes, images & networks"

  echo ""
  echo "Project Command flags:"
  echo "$alias $command $project -l <service>     Log service:        docker-compose logs -f <service>"
  echo "$alias $command $project -x <service>     Exec into service:  docker-compose exec <service> bash"
  echo "$alias $command $project -s               List services"
  echo ""
}

function container_help() {
  echo "Service Commands:"
  echo "$alias l $project <service>       	Log service:        docker-compose logs -f <service>"
  echo "$alias x $project <service>       	Exec into service:  docker-compose exec <service> bash"
  echo ""
  echo "These commands can also be called via flags on Project commands:"
  echo "$alias <command> $project -l <service>   Log service:         docker-compose logs -f <service>"
  echo "$alias <command> $project -x <service>   Exec into service:   docker-compose exec <service> bash"
  echo ""
}

# Find docker project
function _go_to_project() {
  local check=${*%/}
  local directory

  if [[ $check == "." ]] || [[ $check == "" ]]; then
    echo pwd
    exit 1
  fi

  directory=$(find "$HOME" -maxdepth $depth -type d -name "$check" | head -n 1) # force find to exit on first result

  if test -z "$directory"; then
    echo "'$check' is not a directory" >&2
    exit 1
  else
    echo "${directory}"
  fi
}

### PRIVATE ###

function _project_command_help() {
  while getopts ":ha" opt; do
    case ${opt} in
    h)
      proj_help
      exit 0
      ;;
    a)
      all_projects=true
      echo "running command against all listed projects"
      shift $((OPTIND - 1))
      ;;
    \?)
      echo "Invalid Option: -$OPTARG" 1>&2
      exit 1
      ;;
    esac
  done
}

function _container_command_help() {
  while getopts ":h" opt; do
    case ${opt} in
    h)
      cont_help
      exit 0
      ;;
    \?)
      echo "Invalid Option: -$OPTARG" 1>&2
      exit 1
      ;;
    esac
  done
  shift $((OPTIND - 1))
}

function _dn() {
  docker-compose down --remove-orphans
}
function _up() {
  docker-compose up -d
}
function _b() {
  docker-compose build
}
function _log() {
  docker-compose logs -f --tail="100" $1
}
function _exec() {
  docker-compose exec $1 bash
}
function _project_command() {
  while getopts "lxhs" opt; do
    case ${opt} in
    h)
      proj_help
      cont_help
      exit 0
      ;;
    l)
      echo "logging: $OPTARG"
      _log "$OPTARG"
      ;;
    x)
      echo "execing into: $OPTARG"
      _exec "$OPTARG"
      ;;
    s)
      project=$1
      shift
      cd "$(_go_to_project "$project")" || return
      docker-compose ps --services
      ;;
    \?)
      echo "Invalid Option: -$OPTARG" 1>&2
      exit 1
      ;;
    :)
      echo "Invalid Option: -$OPTARG requires an container" 1>&2
      echo "containers:"
      docker-compose ps --services
      exit 1
      ;;
    esac
    shift $((OPTIND - 1))
  done
}

# --- Helpers end --- #

while getopts "h" opt; do
  case ${opt} in
  h)
    script_help
    exit 0
    ;;
  \?)
    echo "Invalid Option: -$OPTARG" 1>&2
    exit 1
    ;;
  esac
  shift $((OPTIND - 1))
done

command=$1
shift # Remove alias from the argument list

case "$command" in
# Parse options to the install sub command
ls | list)
  _project_command_help "$@"
  project=$1
  shift
  cd "$(_go_to_project "$project")" || return
  docker-compose ps --services
  ;;
up)
  _project_command_help $@
  if $all_projects; then
    shift
    for var in "$@"; do
      cd "$(_go_to_project "$var")" || return
      _up
    done
  else
    project=$1
    shift
    cd "$(_go_to_project "$project")" || return
    _up && _project_command "$@"
  fi
  ;;
dn | down)
  _project_command_help "$@"
  if $all_projects; then
    shift
    for var in "$@"; do
      cd "$(_go_to_project "$var")" || return
      _dn
    done
  else
    project=$1
    shift
    cd "$(_go_to_project "$project")" || return
    _dn
  fi
  ;;
b | build)
  _project_command_help "$@"
  if $all_projects; then
    shift
    for var in "$@"; do
      cd "$(_go_to_project "$var")" || return
      _b
    done
  else
    project=$1
    shift
    cd "$(_go_to_project "$project")" || return
    _b && _project_command "$@"
  fi
  ;;
rs | restart)
  _project_command_help "$@"
  if $all_projects; then
    shift
    for var in "$@"; do
      cd "$(_go_to_project "$var")" || return
      _dn && _up
    done
  else
    project=$1
    shift
    cd "$(_go_to_project "$project")" || return
    _dn && _up && _project_command "$@"
  fi
  ;;
rb | rebuild)
  _project_command_help "$@"
  if $all_projects; then
    shift
    for var in "$@"; do
      cd "$(_go_to_project "$var")" || return
      _dn && _b && _up
    done
  else
    project=$1
    shift
    cd "$(_go_to_project "$project")" || return
    _dn && _b && _up && _project_command "$@"
  fi
  ;;
l | logs)
  _container_command_help "$@"
  if [[ $# -ne 1 ]]; then
    project=$1
    shift
    cd "$(_go_to_project "$project")" || return
  else
    cd pwd || return
  fi
  _log $1
  ;;
x | exec)
  _container_command_help "$@"
  if [[ $# -ne 1 ]]; then
    project=$1
    shift
    cd "$(_go_to_project "$project")" || return
  else
    cd pwd || return
  fi
  _exec "$1"
  ;;
nuke)
  _project_command_help "$@"
  if $all_projects; then
    shift
    for var in "$@"; do
      cd "$(_go_to_project "$var")" || return
      _dn && docker system prune && docker volume prune -f && _b && _up
    done
  else
    project=$1
    shift
    cd "$(_go_to_project "$project")" || return
    _dn && docker system prune && docker volume prune -f && _b && _up && _project_command "$@"
  fi
  ;;
update)
  shift
  cd $HOME/scripts ||return
  git fetch --tags --force
  latesttag=$(git tag --list '[vV]*' --sort=v:refname |tail -1)
  echo checking out "${latesttag}"
  git checkout "${latesttag}"
  # alias dev script
  source "$HOME"/.bashrc
  ;;
v | version)
  shift
  cd $HOME/scripts ||return
  echo "version: $(git describe --tags)" 
  exit 0
  ;;
*)
  script_help
  echo "Error:"
  echo "'$command' is not a known command" >&2
  exit 1
  ;;
esac
